#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-

'''
Usage:
    solver.py   (-h | --help)
    solver.py   --version
    solver.py  <ks_file>
    solver.py  <ks_file> [-a <algorithm>]

Arguments:
    ks_file : knapsack file
        The full path to a file defining a knapsack problem.
        Please see sample_files for sample files.

Options:
    -h --help   Show this screen.
    --version   Show the version.
    -a=<algorithm>  [default: 3]
        The algorithm to be used in solving the knapsack problem:
        0: Greedy algorithm
        1: Best first greedy algorithm
        2: Dynamic programming
        3: Branch and bound
        4: MIP with Gurobi [Note: Requires Gurobi license]

Returns:
    The output of the knapsack solver.
    The first line gives the value of the knapsack as well as a binary value
    indicating whether the solution is optimal.
    The second line is a binary string of the same length as the number of
    items in the input file. 1 indicates that the item in the corresponding
    position was included in the knapsack.
'''


if __name__ == '__main__':

    from docopt import docopt

    from utils import parse, format_solution
    from algorithm_branch_and_bound import branch_and_bound
    from algorithm_greedy import greedy_simple, greedy_best_first
    from algorithm_dynamic import dynamic_programming
    from algorithm_mip import mip

    arguments = docopt(__doc__, options_first=False, version='0.1')

    INFILE = arguments['<ks_file>']

    algorithm = int(arguments['-a'])

    with open(INFILE, 'r') as input_data_file:
        input_data = input_data_file.read()

    capacity, items = parse(input_data)

    algo = {
        # key: algorithm_function
        0: greedy_simple,
        1: greedy_best_first,
        2: dynamic_programming,
        3: branch_and_bound,
        4: mip
    }

    optimizer = algo.get(algorithm)

    value, taken, optimality = optimizer(capacity, items)
    print(format_solution(value, taken, optimality))