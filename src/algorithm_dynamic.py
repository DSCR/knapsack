# -*- coding: utf-8 -*-


def dynamic_programming(capacity, items):
    '''A dynamic programming algorithm to solve the knapsack problem.

    Arguments
    ---------
    capacity : int
        The capacity of the Knapsack.
    items : list
        The list of items, each of which is a namedtuple with attributes weight
        and value (and index).

    Returns
    -------
    value : int
        The value of the knapsack.
    taken : list
        A list of binary values of the same length as the number of items.
        A 1 indicates that the item in the corresponding position is included
        in the knapsack.
    optimality : int
        A binary value indicating whether the solution is proven optimal.
    '''

    item_count = len(items)
    value = 0
    weight = 0
    taken = [0] * len(items)

    K = [[0 for x in range(capacity + 1)] for x in range(item_count + 1)]

    # Build table K[][] in bottom up manner
    for i in range(item_count + 1):
        for w in range(capacity + 1):
            if i == 0 or w == 0:
                K[i][w] = 0
            elif items[i - 1].weight <= w:
                K[i][w] = max(items[i - 1].value + K[i - 1][w - items[i - 1].weight], K[i - 1][w])
            else:
                K[i][w] = K[i - 1][w]

    # Trace back to get items
    M = capacity
    for n in range(item_count + 1)[::-1]:
        if K[n][M] != K[n - 1][M]:
            item = items[n - 1]
            taken[item.index] = 1
            value += item.value
            weight += item.weight
            M -= item.weight

    assert weight <= capacity, 'Not a valid solution. Weight returned exceeds capacity!'

    return value, taken, 1