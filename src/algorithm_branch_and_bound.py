# -*- coding: utf-8 -*-

import queue


def branch_and_bound(capacity, items):
    '''A branch and bound algorithm to solve the knapsack problem.

    Arguments
    ---------
    capacity : int
        The capacity of the Knapsack.
    items : list
        The list of items, each of which is a namedtuple with attributes weight
        and value (and index).

    Returns
    -------
    value : int
        The value of the knapsack.
    taken : list
        A list of binary values of the same length as the number of items.
        A 1 indicates that the item in the corresponding position is included
        in the knapsack.
    optimality : int
        A binary value indicating whether the solution is proven optimal.
    '''

    # sort items by value density
    items.sort(key=lambda k: float(k.value) / k.weight, reverse=True)

    # create an empty queue
    q = queue.Queue()

    # create a dummy node for the root of the tree, add to queue
    root = Node(0, 0, 0, 0.0, [])
    root.bound = _upper_bound(root, capacity, items)
    q.put(root)

    maxValue = 0
    best = set()

    while not q.empty():

        # extract an item from the queue
        c = q.get()

        # print(q.qsize())

        if c.bound > maxValue:
            level = c.level + 1
            # print('level: ', level)

        # check 'left' node (the case in which the item is added to the knapsack)
        left = Node(level, c.value + items[level - 1].value, c.weight + items[level - 1].weight, 0.0, c.contains[:])
        left.bound = _upper_bound(left, capacity, items)
        left.contains.append(level)

        if left.weight <= capacity:
            if left.value > maxValue:
                maxValue = left.value
                best = set(left.contains)
            # If the upper bound exceeds the currently best value (e.g. we have
            # hope of finding something even better by following the tree)
            if left.bound > maxValue:
                q.put(left)

        # check 'right' node (the case in which the items is not added to the knapsack)
        right = Node(level, c.value, c.weight, 0.0, c.contains[:])
        right.bound = _upper_bound(right, capacity, items)
        if right.weight <= capacity:
            if right.value > maxValue:
                maxValue = right.value
                best = set(right.contains)
            if right.bound > maxValue:
                q.put(right)

    value = 0
    weight = 0
    taken = [0] * len(items)
    for b in best:
        _id = items[b - 1].index
        taken[_id] = 1
        value += items[b - 1].value
        weight += items[b - 1].weight

    assert weight <= capacity, 'Not a valid solution. Weight returned exceeds capacity!'

    return value, taken, 1


class Node:

    def __init__(self, level, value, weight, bound, contains):
        self.level = level
        self.value = value
        self.weight = weight
        self.bound = bound
        self.contains = contains


def _upper_bound(node, capacity, items):
    item_count = len(items)
    if node.weight > capacity:
        return 0
    else:
        bound = node.value
        wt = node.weight
        j = node.level

        while j < item_count and wt + items[j].weight <= capacity:
            bound += items[j].value
            wt += items[j].weight
            j += 1
        if j < item_count:
            bound += (capacity - wt) * float(items[j].value) / items[j].weight
        return bound
