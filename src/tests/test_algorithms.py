# -*- coding: utf-8 -*-
from .context import parse

from .context import greedy_simple, greedy_best_first
from .context import dynamic_programming
from .context import branch_and_bound
# from .context import mip

# from . import helpers

import unittest
import os

here = os.path.split(__file__)[0]
data_dir = os.path.join(here, 'data')


SAMPLE_FILE1 = os.path.join(data_dir, 'knapsack_19')
with open(SAMPLE_FILE1, 'r') as f:
    sample_data = f.read()


class AlgorithmTests(unittest.TestCase):

    '''
    '''

    def setUp(self):
        self.capacity, self.items = parse(sample_data)

    def test_greedy_simple(self):
        value_exp = 5296
        taken_exp = [1, 1, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        optimality_exp = 0

        value, taken, optimality = greedy_simple(self.capacity, self.items)

        self.assertEqual(value, value_exp)
        self.assertEqual(taken, taken_exp)
        self.assertEqual(optimality, optimality_exp)

    def test_greedy_best_first(self):
        value_exp = 6724
        taken_exp = [1, 0, 0, 0, 1, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0]
        optimality_exp = 0

        value, taken, optimality = greedy_best_first(self.capacity, self.items)

        self.assertEqual(value, value_exp)
        self.assertEqual(taken, taken_exp)
        self.assertEqual(optimality, optimality_exp)

    def test_dynamic_programming(self):
        value_exp = 6765
        taken_exp = [0, 1, 0, 0, 1, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 0]
        optimality_exp = 1

        value, taken, optimality = dynamic_programming(self.capacity, self.items)

        self.assertEqual(value, value_exp)
        self.assertEqual(taken, taken_exp)
        self.assertEqual(optimality, optimality_exp)

    def test_branch_and_bound(self):
        value_exp = 6765
        taken_exp = [0, 1, 0, 0, 1, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 0]
        optimality_exp = 1

        value, taken, optimality = branch_and_bound(self.capacity, self.items)

        self.assertEqual(value, value_exp)
        self.assertEqual(taken, taken_exp)
        self.assertEqual(optimality, optimality_exp)

    # def test_mip(self):
    #     value_exp = 6765
    #     taken_exp = [0, 1, 0, 0, 1, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 0]
    #     optimality_exp = 1

    #     value, taken, optimality = mip(self.capacity, self.items)

    #     self.assertEqual(value, value_exp)
    #     self.assertEqual(taken, taken_exp)
    #     self.assertEqual(optimality, optimality_exp)


if __name__ == '__main__':
    unittest.main()