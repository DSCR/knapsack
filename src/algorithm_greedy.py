# -*- coding: utf-8 -*-


def greedy_simple(capacity, items):
    '''A greedy algorithm to solve the knapsack problem.

    Arguments
    ---------
    capacity : int
        The capacity of the Knapsack.
    items : list
        The list of items, each of which is a namedtuple with attributes weight
        and value (and index).

    Returns
    -------
    value : int
        The value of the knapsack.
    taken : list
        A list of binary values of the same length as the number of items.
        A 1 indicates that the item in the corresponding position is included
        in the knapsack.
    optimality : int
        A binary value indicating whether the solution is proven optimal.
    '''

    # a trivial greedy algorithm for filling the knapsack
    # it takes items in-order until the knapsack is full
    value = 0
    weight = 0
    taken = [0] * len(items)

    for item in items:
        if weight + item.weight <= capacity:
            taken[item.index] = 1
            value += item.value
            weight += item.weight

    return value, taken, 0


def greedy_best_first(capacity, items):
    '''A greedy algorithm to solve the knapsack problem. Items are added to the
    knapsack in order of decreasing value density until the knapsack is full.

    Arguments
    ---------
    capacity : int
        The capacity of the Knapsack.
    items : list
        The list of items, each of which is a namedtuple with attributes weight
        and value (and index).

    Returns
    -------
    value : int
        The value of the knapsack.
    taken : list
        A list of binary values of the same length as the number of items.
        A 1 indicates that the item in the corresponding position is included
        in the knapsack.
    optimality : int
        A binary value indicating whether the solution is proven optimal.
    '''

    # sort items by value density
    items.sort(key=lambda k: float(k.value) / k.weight, reverse=True)

    value = 0
    weight = 0
    taken = [0] * len(items)

    for item in items:
        if weight + item.weight <= capacity:
            taken[item.index] = 1
            value += item.value
            weight += item.weight

    return value, taken, 0