# -*- coding: utf-8 -*-
from collections import namedtuple


def parse(input_data):
    '''Parse raw input data and return capacity and items objects as input for
    optimizer.
    '''

    Item = namedtuple('Item', ['index', 'value', 'weight'])

    # parse the input
    lines = input_data.split('\n')

    firstLine = lines[0].split()
    item_count = int(firstLine[0])
    capacity = int(firstLine[1])

    items = []

    for i in range(1, item_count + 1):
        line = lines[i]
        parts = line.split()
        items.append(Item(i - 1, int(parts[0]), int(parts[1])))

    return capacity, items


def format_solution(value, taken, optimality=0):
    '''Format output of optimizer.
    '''
    output_data = str(value) + ' ' + str(optimality) + '\n'
    output_data += ' '.join(map(str, taken))
    return output_data