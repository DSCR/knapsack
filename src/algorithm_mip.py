# -*- coding: utf-8 -*-
import gurobipy as gp
from gurobipy import GRB, quicksum


def mip(capacity, items):
    '''A mixed integer model to solve the knapsack problem using Gurobi.

    Arguments
    ---------
    capacity : int
        The capacity of the Knapsack.
    items : list
        The list of items, each of which is a namedtuple with attributes weight
        and value (and index).

    Returns
    -------
    value : int
        The value of the knapsack.
    taken : list
        A list of binary values of the same length as the number of items.
        A 1 indicates that the item in the corresponding position is included
        in the knapsack.
    optimality : int
        A binary value indicating whether the solution is proven optimal.
    '''

    keys = range(len(items))
    weights = [i.weight for i in items]
    values = [i.value for i in items]

    # Instantiate a mip model
    m = gp.Model('Knapsack')

    # Set parameters
    m.setParam('OutputFlag', 0)  # quiet mode

    # Define decision variables
    included = m.addVars(keys, vtype=GRB.BINARY, name='item in knapsack')

    # Define constraint(s)
    m.addConstr((quicksum([weights[i] * included[i] for i in keys]) <= capacity), name='knapsack capacity')

    # Define objective
    m.setObjective(quicksum([values[i] * included[i] for i in keys]), GRB.MAXIMIZE)

    # Optimize
    m.optimize()

    taken = [1 if included[i].x > 1e-6 else 0 for i in keys]

    if m.status == 2:
        optimality = 1
    else:
        optimality = 0

    return int(m.objVal), taken, optimality