# Knapsack problem

The knapsack problem is described by a knapsack (backpack) of finite
weight-capacity and a set of objects of different weights and values. The
objective is to determine which objects to pack into the knapsack so as to
maximize their combined value whilst not exceeding weight-capacity of the
knapsack.


## About this solution

Five different algorithms are implemented and made available through the
command line script, ``solver.py``. Two of these are simple heuristic algorithms which
are not guaranteed to give an optimal solution. The remaining three are exact
and thus capable, at least in principle, of reaching optimality. The algorithms
are as follows:

<ol start="0">
  <li>Simple greedy algorithm.</li>
  <li>Best first greedy algorithm.</li>
  <li>Dynamic programming.</li>
  <li>Branch and bound.</li>
  <li>Mixed integer programming (MIP) (with Gurobi).</li>
</ol>

The choice of algorithm is passed to the command line script via the ``-a`` flag.

Please note, in order to try the MIP model, a valid Gurobi license is required.

The code has been tested with ``python3.7``, but is likely to work also with later versions.


## Getting started
```
python -m pip install -r requirements.txt
python -m pytest
```


## Usage
```
python src/solver.py -h
```

```
python src/solver.py src/sample_files/knapsack_19 -a 3
```